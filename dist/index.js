'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.imageSnapshot = undefined;

var _extends2 = require('babel-runtime/helpers/extends');

var _extends3 = _interopRequireDefault(_extends2);

var _regenerator = require('babel-runtime/regenerator');

var _regenerator2 = _interopRequireDefault(_regenerator);

var _asyncToGenerator2 = require('babel-runtime/helpers/asyncToGenerator');

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

var _puppeteer = require('puppeteer');

var _puppeteer2 = _interopRequireDefault(_puppeteer);

var _jestImageSnapshot = require('jest-image-snapshot');

var _nodeLogger = require('@storybook/node-logger');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

expect.extend({ toMatchImageSnapshot: _jestImageSnapshot.toMatchImageSnapshot });

// We consider taking the full page is a reasonnable default.
var defaultScreenshotOptions = function defaultScreenshotOptions() {
  return { fullPage: true };
};

var noop = function noop() {};
var asyncNoop = function () {
  var _ref = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee() {
    return _regenerator2.default.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
          case 'end':
            return _context.stop();
        }
      }
    }, _callee, undefined);
  }));

  return function asyncNoop() {
    return _ref.apply(this, arguments);
  };
}();

var defaultConfig = {
  storybookUrl: 'http://localhost:6006',
  chromeExecutablePath: undefined,
  getMatchOptions: noop,
  getScreenshotOptions: defaultScreenshotOptions,
  beforeScreenshot: noop,
  getGotoOptions: noop,
  customizePage: asyncNoop
};

var imageSnapshot = exports.imageSnapshot = function imageSnapshot() {
  var customConfig = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};

  var _defaultConfig$custom = (0, _extends3.default)({}, defaultConfig, customConfig),
      storybookUrl = _defaultConfig$custom.storybookUrl,
      chromeExecutablePath = _defaultConfig$custom.chromeExecutablePath,
      getMatchOptions = _defaultConfig$custom.getMatchOptions,
      getScreenshotOptions = _defaultConfig$custom.getScreenshotOptions,
      beforeScreenshot = _defaultConfig$custom.beforeScreenshot,
      getGotoOptions = _defaultConfig$custom.getGotoOptions,
      customizePage = _defaultConfig$custom.customizePage;

  var browser = void 0; // holds ref to browser. (ie. Chrome)
  var page = void 0; // Hold ref to the page to screenshot.

  var testFn = function () {
    var _ref2 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee2(_ref3) {
      var context = _ref3.context;
      var kind, framework, story, encodedKind, encodedStoryName, storyUrl, url, image;
      return _regenerator2.default.wrap(function _callee2$(_context2) {
        while (1) {
          switch (_context2.prev = _context2.next) {
            case 0:
              kind = context.kind, framework = context.framework, story = context.story;

              if (!(framework === 'rn')) {
                _context2.next = 4;
                break;
              }

              // Skip tests since we de not support RN image snapshots.
              _nodeLogger.logger.error("It seems you are running imageSnapshot on RN app and it's not supported. Skipping test.");

              return _context2.abrupt('return');

            case 4:
              encodedKind = encodeURIComponent(kind);
              encodedStoryName = encodeURIComponent(story);
              storyUrl = '/iframe.html?selectedKind=' + encodedKind + '&selectedStory=' + encodedStoryName;
              url = storybookUrl + storyUrl;

              if (!(!browser || !page)) {
                _context2.next = 11;
                break;
              }

              _nodeLogger.logger.error('Error when generating image snapshot for test ' + kind + ' - ' + story + ' : It seems the headless browser is not running.');

              throw new Error('no-headless-browser-running');

            case 11:

              expect.assertions(1);

              _context2.prev = 12;
              _context2.next = 15;
              return customizePage(page);

            case 15:
              _context2.next = 17;
              return page.goto(url, getGotoOptions({ context: context, url: url }));

            case 17:
              _context2.next = 19;
              return beforeScreenshot(page, { context: context, url: url });

            case 19:
              _context2.next = 21;
              return page.screenshot(getScreenshotOptions({ context: context, url: url }));

            case 21:
              image = _context2.sent;


              expect(image).toMatchImageSnapshot(getMatchOptions({ context: context, url: url }));
              _context2.next = 29;
              break;

            case 25:
              _context2.prev = 25;
              _context2.t0 = _context2['catch'](12);

              _nodeLogger.logger.error('ERROR WHILE CONNECTING TO ' + url + ', did you start or build the storybook first ? A storybook instance should be running or a static version should be built when using image snapshot feature.', _context2.t0);
              throw _context2.t0;

            case 29:
            case 'end':
              return _context2.stop();
          }
        }
      }, _callee2, undefined, [[12, 25]]);
    }));

    return function testFn(_x2) {
      return _ref2.apply(this, arguments);
    };
  }();

  testFn.afterAll = function () {
    return browser.close();
  };

  testFn.beforeAll = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee3() {
    return _regenerator2.default.wrap(function _callee3$(_context3) {
      while (1) {
        switch (_context3.prev = _context3.next) {
          case 0:
            _context3.next = 2;
            return _puppeteer2.default.launch({
              args: ['--no-sandbox ', '--disable-setuid-sandbox', '--disable-dev-shm-usage'],
              executablePath: chromeExecutablePath
            });

          case 2:
            browser = _context3.sent;
            _context3.next = 5;
            return browser.newPage();

          case 5:
            page = _context3.sent;

          case 6:
          case 'end':
            return _context3.stop();
        }
      }
    }, _callee3, undefined);
  }));

  return testFn;
};